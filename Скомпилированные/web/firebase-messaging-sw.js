importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/8.10.0/firebase-messaging.js");

firebase.initializeApp({
    apiKey: 'AIzaSyBS00rLcyNtyE1HGhLXVXPXsLJsmBA-CvY',
    appId: '1:1039607715936:web:dff1150bfa04aedc66cb05',
    messagingSenderId: '1039607715936',
    projectId: 'itdt-catalog-demo-home',
    authDomain: 'itdt-catalog-demo-home.firebaseapp.com',
    storageBucket: 'itdt-catalog-demo-home.appspot.com',
    measurementId: 'G-XEHVR05W15',
});

const messaging = firebase.messaging();

// Optional:
messaging.onBackgroundMessage((message) => {
  console.log("onBackgroundMessage", message);
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
  };

  self.registration.showNotification(notificationTitle, notificationOptions);
});